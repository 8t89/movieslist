package com.ac.movieslist.features.movies.repository

import com.ac.movieslist.data.Result
import com.ac.movieslist.data.api.MoviesApi
import com.ac.movieslist.data.api.models.base.Response
import com.ac.movieslist.data.local.MovieDao
import com.ac.movieslist.data.local.models.Movie
import com.ac.movieslist.utils.MockDataLoader
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import com.ac.movieslist.data.api.models.Movie as ApiMovie

class AppMoviesRepositoryTest {

    private lateinit var mockList: List<Movie>
    private lateinit var mockListTwo: List<Movie>
    private lateinit var mockApiResponse: Response<List<ApiMovie>>
    @Mock
    lateinit var movieDao: MovieDao

    @Mock
    lateinit var movieApi: MoviesApi

    lateinit var repository: MoviesRepository

    private val mockDatLoader: MockDataLoader = MockDataLoader()

    @Before
    fun before() {
        initMockData()
        MockitoAnnotations.initMocks(this)
        repository = AppMoviesRepository(movieApi, movieDao)
    }

    private fun initMockData() {
        mockList = mockDatLoader.getMockedMoviesList()
        mockListTwo = mockDatLoader.getMockedMoviesListTwo()
        mockApiResponse = mockDatLoader.getMockedMoviesResponse()
    }

    @Test
    fun getTopMoviesFromDb() {
        Mockito.doReturn(Single.just(mockList))
            .`when`(movieDao).getPaginatedMovies(anyInt(), anyInt())
        Mockito.doReturn(Single.just(mockListTwo))
            .`when`(movieApi).getTopMovies(anyInt())
        val testObserver = repository.getTopMovies(0).test()
        testObserver.assertValueAt(0, Result.loading())
        testObserver.assertValueAt(1, Result.success(mockList))
    }

    @Test
    fun getMoviesFromApi() {
        Mockito.doReturn(Single.just(listOf<Movie>()))
            .`when`(movieDao).getPaginatedMovies(anyInt(), anyInt())
        Mockito.doReturn(Single.just(mockApiResponse))
            .`when`(movieApi).getTopMovies(anyInt())
        val testObserver = repository.getTopMovies(0).test()
        testObserver.assertValueAt(0, Result.loading())
        testObserver.assertValueAt(
            1,
            Result.success(mockApiResponse.results.map { it.toDbMovie() })
        )
    }


}