package com.ac.movieslist.features.movies.list

import RxImmediateSchedulerRule
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ac.movieslist.data.Result
import com.ac.movieslist.data.local.models.Movie
import com.ac.movieslist.features.movies.repository.MoviesRepository
import com.ac.movieslist.utils.MockDataLoader
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.mockito.*

class MoviesListViewModelTest {

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Captor
    private val errorCaptor: ArgumentCaptor<String>? = null
    private val errorObserver: Observer<String> = mock()

    @Captor
    private val moviesCaptor: ArgumentCaptor<List<Movie>>? = null
    private val moviesObserver: Observer<List<Movie>> = mock()

    @Captor
    private val loadingCaptor: ArgumentCaptor<Boolean>? = null
    private val loadingObserver: Observer<Boolean> = mock()

    private lateinit var mockList: List<Movie>

    @Mock
    lateinit var moviesRepository: MoviesRepository

    lateinit var viewModel: MoviesListViewModel

    private val mockDatLoader: MockDataLoader = MockDataLoader()

    @Before
    fun before() {
        initMockData()
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        MockitoAnnotations.initMocks(this)
        viewModel = MoviesListViewModel(moviesRepository)
        viewModel.error.observeForever(errorObserver)
        viewModel.isLoading.observeForever(loadingObserver)
        viewModel.movies.observeForever(moviesObserver)
    }

    private fun initMockData() {
        mockList = mockDatLoader.getMockedMoviesList()
    }

    @Test
    fun onFragmentAttached() {
        Mockito.doReturn(Observable.just(Result.success(mockList))).`when`(moviesRepository)
            .getTopMovies(ArgumentMatchers.anyInt())
        viewModel.onFragmentAttached()
        moviesCaptor.run {
            verify(moviesObserver, times(2)).onChanged(moviesCaptor?.capture())
            assertEquals(mockList, this?.value)
        }

    }

    @Test
    fun fetchDataAtPageSuccess() {
        Mockito.doReturn(Observable.just(Result.success(mockList))).`when`(moviesRepository)
            .getTopMovies(ArgumentMatchers.anyInt())
        viewModel.fetchDataAtPage(0)
        moviesCaptor.run {
            verify(moviesObserver, times(2)).onChanged(moviesCaptor?.capture())
            assertEquals(mockList, this?.value)
        }
    }

    @Test
    fun fetchDataAtPageError() {
        Mockito.doReturn(Observable.just(Result.error<List<Movie>>("Error")))
            .`when`(moviesRepository)
            .getTopMovies(ArgumentMatchers.anyInt())
        viewModel.fetchDataAtPage(0)
        moviesCaptor.run {
            verify(moviesObserver, times(1)).onChanged(moviesCaptor?.capture())
            assertEquals(mutableListOf<Movie>(), this?.value)
        }
        errorCaptor.run {
            verify(errorObserver, times(1)).onChanged(errorCaptor?.capture())
            assertEquals("Error", this?.value)
        }
    }

    @Test
    fun fetchDataLoading() {
        Mockito.doReturn(Observable.just(Result.loading<List<Movie>>())).`when`(moviesRepository)
            .getTopMovies(ArgumentMatchers.anyInt())
        viewModel.fetchDataAtPage(0)
        loadingCaptor.run {
            verify(loadingObserver, times(2)).onChanged(loadingCaptor?.capture())
            assertEquals(true, this?.value)
        }
    }


}