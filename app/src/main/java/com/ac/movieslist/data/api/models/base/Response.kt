package com.ac.movieslist.data.api.models.base

data class Response<T>(
    val page: Int?,
    val total_results: Int?,
    val total_pages: Int?,
    val results: T
)