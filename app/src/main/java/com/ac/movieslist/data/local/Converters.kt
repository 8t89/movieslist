package com.ac.movieslist.data.local

import androidx.room.TypeConverter

class Converters {
    @TypeConverter
    fun intListFromString(string: String?) = string?.split(SEPARATOR)?.map { it.toInt() }

    @TypeConverter
    fun intListToString(list: List<Int>?) = list?.joinToString(SEPARATOR)

    companion object {
        private const val SEPARATOR = "###"
    }

}