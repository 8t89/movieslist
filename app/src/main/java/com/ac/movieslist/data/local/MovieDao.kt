package com.ac.movieslist.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ac.movieslist.data.local.models.Movie
import io.reactivex.Single

@Dao
abstract class MovieDao {
    @Insert
    abstract fun insert(movie: Movie)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertAll(recipes: List<Movie>)

    @Query("SELECT * FROM movies ORDER BY popularity DESC LIMIT :limit OFFSET :offset")
    abstract fun getPaginatedMovies(limit: Int, offset: Int): Single<List<Movie>>
}