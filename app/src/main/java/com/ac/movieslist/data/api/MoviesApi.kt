package com.ac.movieslist.data.api

import com.ac.movieslist.data.api.models.Movie
import com.ac.movieslist.data.api.models.base.Response
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesApi {
    @GET("movie/popular")
    fun getTopMovies(@Query("page") page: Int): Single<Response<List<Movie>>>
}