package com.ac.movieslist.data.api

import com.ac.movieslist.BuildConfig

object ImageUtils {
    @JvmStatic
    fun getImagePath(imageUrl: String?): String {
        return "${BuildConfig.IMAGES_URL}$imageUrl"
    }
}