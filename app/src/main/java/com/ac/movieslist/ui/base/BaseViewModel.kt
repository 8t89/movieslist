package com.ac.movieslist.ui.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()