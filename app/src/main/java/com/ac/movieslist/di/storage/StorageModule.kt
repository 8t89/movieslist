package com.ac.movieslist.di.storage

import android.app.Application
import androidx.room.Room
import com.ac.movieslist.data.local.AppDatabase
import com.ac.movieslist.data.local.MovieDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Singleton
    @Provides
    fun providesRoomDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder<AppDatabase>(application, AppDatabase::class.java, "recipe-db")
            .build()
    }

    @Singleton
    @Provides
    fun provideRecipesDao(appDatabase: AppDatabase): MovieDao {
        return appDatabase.movieDao()
    }
}