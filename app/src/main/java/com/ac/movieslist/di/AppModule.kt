package com.ac.movieslist.di

import dagger.Module

@Module(includes = [ApplicationModuleBinds::class])
open class AppModule

@Module
abstract class ApplicationModuleBinds