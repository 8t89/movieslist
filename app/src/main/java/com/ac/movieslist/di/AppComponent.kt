package com.ac.movieslist.di

import android.app.Application
import com.ac.movieslist.MoviesListApp
import com.ac.movieslist.di.network.NetworkModule
import com.ac.movieslist.di.storage.StorageModule
import com.ac.movieslist.features.movies.MoviesModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        AndroidSupportInjectionModule::class,
        NetworkModule::class,
        StorageModule::class,
        MoviesModule::class
    ]
)
interface AppComponent : AndroidInjector<MoviesListApp> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}