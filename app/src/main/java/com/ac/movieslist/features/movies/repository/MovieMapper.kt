package com.ac.movieslist.features.movies.repository

import com.ac.movieslist.data.api.models.Movie as ApiMovie
import com.ac.movieslist.data.local.models.Movie as DbMovie

fun ApiMovie.toDbMovie(): DbMovie = DbMovie(
    adult,
    backdrop_path,
    genre_ids,
    id,
    original_language,
    original_title,
    overview,
    popularity,
    poster_path,
    release_date,
    title,
    video,
    vote_average,
    vote_count
)