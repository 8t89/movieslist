package com.ac.movieslist.features.movies.repository

import com.ac.movieslist.data.Result
import com.ac.movieslist.data.api.MoviesApi
import com.ac.movieslist.data.local.MovieDao
import io.reactivex.Observable
import javax.inject.Inject
import com.ac.movieslist.data.local.models.Movie as DbMovie

class AppMoviesRepository @Inject constructor(
    private val moviesApi: MoviesApi,
    private val movieDao: MovieDao
) : MoviesRepository {

    override fun getTopMovies(page: Int): Observable<Result<List<DbMovie>>> {
        return Observable.concat(
            Observable.just(Result.loading()),
            createDataObservables(page)
        ).onErrorReturn { Result.error(it.localizedMessage) }
    }

    private fun createDataObservables(page: Int): Observable<Result<List<DbMovie>>> {
        return Observable.concat(
            createDatabaseObservable(page),
            createApiObservable(page)
        )
            .filter { it.isNotEmpty() }
            .first(emptyList())
            .toObservable()
            .map {
                Result.success(it)
            }
    }

    private fun createDatabaseObservable(page: Int): Observable<List<DbMovie>> {
        return movieDao
            .getPaginatedMovies((page - 1) * MOVIES_PER_PAGE, (page) * MOVIES_PER_PAGE)
            .onErrorReturnItem(emptyList())
            .toObservable()
    }

    private fun createApiObservable(page: Int): Observable<List<DbMovie>> {
        return moviesApi.getTopMovies(page)
            .toObservable()
            .flatMapIterable { it.results }
            .map { it.toDbMovie() }
            .toList()
            .map {
                movieDao.insertAll(it)
                it
            }
            .toObservable()
    }

    companion object {
        const val MOVIES_PER_PAGE = 20
    }
}