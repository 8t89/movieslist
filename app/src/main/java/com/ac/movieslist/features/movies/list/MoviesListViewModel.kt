package com.ac.movieslist.features.movies.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.ac.movieslist.data.Result
import com.ac.movieslist.data.local.models.Movie
import com.ac.movieslist.features.movies.repository.MoviesRepository
import com.ac.movieslist.ui.SingleLiveEvent
import com.ac.movieslist.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MoviesListViewModel @Inject constructor(
    private val moviesRepository: MoviesRepository
) : BaseViewModel() {

    val movies = MutableLiveData<MutableList<Movie>>(mutableListOf())

    val error = SingleLiveEvent<String>()

    private val isLoadingPrivate = MutableLiveData<Boolean>(false)
    var isLoading: LiveData<Boolean>

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    init {
        isLoading = Transformations.map(isLoadingPrivate) {
            it && movies.value.isNullOrEmpty()
        }
    }

    fun fetchDataAtPage(page: Int) {
        compositeDisposable.add(
            moviesRepository.getTopMovies(page + 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    handleMoviesResult(it)
                }, {
                    handleLoadingError(it.localizedMessage)
                })
        )
    }

    private fun handleMoviesResult(result: Result<List<Movie>>) {
        when (result.status) {
            Result.Status.LOADING -> handleLoadingLiveData()
            Result.Status.SUCCESS -> addMoviesAndPostValue(result)
            Result.Status.ERROR -> handleLoadingError(result.message)
        }
    }

    private fun addMoviesAndPostValue(result: Result<List<Movie>>) {
        isLoadingPrivate.postValue(false)
        val currentList = movies.value ?: mutableListOf()
        result.data?.toMutableList()?.let {
            currentList.addAll(it)
            movies.postValue(currentList)
        }
    }

    private fun handleLoadingError(message: String?) {
        error.postValue(message)
    }

    private fun handleLoadingLiveData() {
        isLoadingPrivate.postValue(true)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    fun onFragmentAttached() {
        if (movies.value.isNullOrEmpty()) {
            fetchDataAtPage(0)
        }
    }
}