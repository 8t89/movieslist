package com.ac.movieslist.features.movies.list

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.ac.movieslist.BR
import com.ac.movieslist.R
import com.ac.movieslist.data.local.models.Movie
import com.ac.movieslist.databinding.FragmentMoviesListBinding
import com.ac.movieslist.ui.base.BaseFragment
import com.ac.movieslist.ui.widget.EndlessRecyclerOnScrollListener
import com.ac.movieslist.ui.widget.GenericAdapter
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class MoviesListFragment : BaseFragment<FragmentMoviesListBinding, MoviesListViewModel>() {

    override val bindingVariable: Int = BR.viewModel

    private lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener

    private val moviesAdapter by lazy {
        GenericAdapter<Movie>(R.layout.view_item_movie, null)
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val layoutResId: Int = R.layout.fragment_movies_list

    override val viewModel: MoviesListViewModel by viewModels { viewModelFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.error.observe(this.viewLifecycleOwner, Observer {
            val errorMessage = it ?: getString(R.string.generic_error)
            Toast.makeText(this.context, errorMessage, Toast.LENGTH_LONG).show()
        })
    }

    private fun setUpRecyclerView() {
        viewDataBinding?.rvMovies?.apply {
            endlessRecyclerOnScrollListener = EndlessScrollListener(layoutManager, 5) {
                viewModel.fetchDataAtPage(it)
            }
            addOnScrollListener(endlessRecyclerOnScrollListener)
            adapter = moviesAdapter
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
        viewModel.onFragmentAttached()
    }

    class EndlessScrollListener(
        layoutManager: RecyclerView.LayoutManager?,
        threshold: Int,
        private val onNewPage: (currentPage: Int) -> Unit
    ) :
        EndlessRecyclerOnScrollListener(layoutManager, threshold) {

        override fun onLoadMore(current_page: Int) {
            onNewPage.invoke(current_page)
        }

    }
}