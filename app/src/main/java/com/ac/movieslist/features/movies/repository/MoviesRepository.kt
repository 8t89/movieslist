package com.ac.movieslist.features.movies.repository

import com.ac.movieslist.data.Result
import com.ac.movieslist.data.local.models.Movie
import io.reactivex.Observable

interface MoviesRepository {
    fun getTopMovies(page: Int): Observable<Result<List<Movie>>>
}