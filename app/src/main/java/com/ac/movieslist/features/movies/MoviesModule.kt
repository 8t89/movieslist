package com.ac.movieslist.features.movies

import androidx.lifecycle.ViewModel
import com.ac.movieslist.di.ViewModelBuilder
import com.ac.movieslist.di.ViewModelKey
import com.ac.movieslist.features.movies.list.MoviesListFragment
import com.ac.movieslist.features.movies.list.MoviesListViewModel
import com.ac.movieslist.features.movies.repository.AppMoviesRepository
import com.ac.movieslist.features.movies.repository.MoviesRepository
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class MoviesModule {
    @ContributesAndroidInjector(
        modules = [ViewModelBuilder::class]
    )
    internal abstract fun moviesListFragment(): MoviesListFragment

    @Binds
    @IntoMap
    @ViewModelKey(MoviesListViewModel::class)
    abstract fun bindViewModel(viewModel: MoviesListViewModel): ViewModel

    @Binds
    abstract fun bindRepository(repo: AppMoviesRepository): MoviesRepository
}