# Movies List app

The app uses :

  * MVVM architecture with Data binding
  * Dagger2 for Dependency Injection
  * Navigation component
  * RxJava for communication between repositories and ViewModels
  
#### ToDos

  * UI Tests
  * Add refresh method and SwipeToRefreshLayout to let the user refresh the whole list
  * Add like button